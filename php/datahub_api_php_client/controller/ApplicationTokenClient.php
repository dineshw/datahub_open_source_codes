<?php
namespace controller;

require 'vendor/autoload.php';
include 'config.php';
include 'model/SessionObject.php';

use model\SessionObject;
use GuzzleHttp\Client;


class ApplicationTokenClient {

  private static $instance;

  function __construct() {

    //session_unset();
    $this->client = new Client([
      'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ),
        'base_uri' => $GLOBALS['BASE_URI'],
        'timeout'  => 2.0,
    ]);

  }

  private function getAuthorizationCode() {

  return base64_encode($GLOBALS['CLIENT_ID'].':'.$GLOBALS['CLIENT_SECRET']);

  }

  public static function getInstance()
  {

    if(!isset(self::$instance)) {

       self::$instance = new self();
    }

    return self::$instance;

  }


public function getApplicationToken() {

  if(isset($_SESSION["datahub_session"])) {


    $sessionObject = $_SESSION["datahub_session"];

    //$sessionObject = $GLOBALS['APP_TOKEN'];

    if($sessionObject->checkSession()) {
      echo "SESSION ACTIVE";
    } else {
      echo "SESSION EXPIRED/DOES NOT EXIST. CREATING A NEW SESSION";
      $result=$this->generateToken();
      echo $result;
    }
  } elseif($this->globalSession()) {

    echo "Global session is active";
    $global=$this->readGlobal();
    $sessionObject = new SessionObject();
    $sessionObject->setAccessToken($global['accessToken']);
    // $GLOBALS['APPLICATION_TOKEN']=$jsonObject->access_token;
    $sessionObject->setTokenExpiryTime($global['expiryTime']);
    $_SESSION["datahub_session"] = $sessionObject; // Set to $GLOBALS
    echo $global['accessToken'];

  } else {

    echo "SESSION EXPIRED/DOES NOT EXIST. CREATING A NEW SESSION";
    $result=$this->generateToken();
    echo $result;

  }

  //return $sessionObject->getAccessToken();

}

private function generateToken(){

  $response = $this->client->request('POST', $GLOBALS['TOKEN_ENDPOINT'], [
    'headers' => ['Authorization' => 'Basic '.$this->getAuthorizationCode()],
    'form_params' =>  [
      'grant_type' => 'client_credentials'
    ]
  ]);


  if($response->getStatusCode()==200) {

    $jsonObject = json_decode($response->getBody());
    echo json_encode($jsonObject);
    $sessionObject = new SessionObject();
    $sessionObject->setAccessToken($jsonObject->access_token);
    // $GLOBALS['APPLICATION_TOKEN']=$jsonObject->access_token;
    $expiryTime=(time() + $jsonObject->expires_in);
    $sessionObject->setTokenExpiryTime($expiryTime);
    $_SESSION["datahub_session"] = $sessionObject; // Set to $GLOBALS


    $global=$this->readGlobal();
    $global['accessToken']=$jsonObject->access_token;
    $global['expiryTime']=$expiryTime;
    $this->writeGlobal($global);


    return "Successfully created session";

  } else {

    return "Something went wrong. Please try again!";

  }
}


/**
    * prevent the instance from being cloned (which would create a second instance of it)
    */
   private function __clone()
   {

   }

   /**
    * prevent from being unserialized (which would create a second instance of it)
    */
   private function __wakeup()
   {

   }


   private function readGlobal() {

     echo "reading Global file";
     $json=json_decode(file_get_contents('global.json'),true);
    return $json;

   }

   private function globalSession() {
    $json=json_decode(file_get_contents('global.json'),true);
    if(($json['accessToken']!="")&&($json['expiryTime']>time())) {
      return true;
    } else return false;


   }


   private function writeGlobal($json) {

     echo "writing global file";
     $fp = fopen('global.json', 'w');
     fwrite($fp, json_encode($json));
     fclose($fp);


   }




}




 ?>
