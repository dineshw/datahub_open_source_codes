<?php

namespace sso;
require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/../model/SessionObject.php';
require __DIR__.'/../config.php';

use GuzzleHttp\Client;
use model\SessionObject;

class HttpClient {

  private static $instance;

public function __construct() {

  $this->client = new Client([
      'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ),
        'base_uri' => $GLOBALS['BASE_URI'],
        'timeout'  => 2.0,
    ]);
}


public static function getInstance()
  {

    if(!isset(self::$instance)) {

       self::$instance = new self();
    }

    return self::$instance;

  }


private function getAuthorizationCode() {

  return base64_encode($GLOBALS['CLIENT_ID'].':'.$GLOBALS['CLIENT_SECRET']);

  }

public function getAccessToken($code) {

  $response = $this->client->request('POST', '/oauth2/token', [
  //'headers' => ['Authorization' => 'Basic '.$this->getAuthorizationCode()],
  // 'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
  'form_params' => [
    'client_id' => $GLOBALS['CLIENT_ID'],
    'client_secret' => $GLOBALS['CLIENT_SECRET'],
    'grant_type' => "authorization_code",
    'code' => $code,
    'redirect_uri'=>$GLOBALS['CALLBACK']
  ]
  ]);

if($response->getStatusCode()==200) {

$json =json_decode($response->getBody());
$session= new SessionObject();
//echo $response->getBody();
$session->setAccessToken($json->access_token);
$session->setRefreshToken($json->refresh_token);
$session->setTokenExpiryTime(time() + $json->expires_in);
$id_token = explode(".", $json->id_token);
$json_token = json_decode(base64_decode($id_token[1]), true);


echo "\nUser Name: ".$json_token['username']."\n";
echo "\nRole :".$json_token['http://wso2.org/claims/role']."\n";
echo "\nFirst Name: ".$json_token['firstName']."\n";
echo "\nLast Name: ".$json_token['lastName']."\n";
echo "\nMobile no: ".$json_token['mobileNo']."\n";
echo "\nEmail: ".$json_token['http://wso2.org/claims/emailaddress']."\n";
echo "\nAuthorized CID: ".$json_token['authorizedCID']."\n";

$session->setUserName($json_token['username']);
$session->setRole($json_token['http://wso2.org/claims/role']);
$session->setFirstName($json_token['firstName']);
$session->setMiddleName($json_token['middleName']);
$session->setLastName($json_token['lastName']);
$session->setMobileNo($json_token['mobileNo']);
$session->setEmail($json_token['http://wso2.org/claims/emailaddress']);
$session->setAuthorizedCID($json_token['authorizedCID']);

$_SESSION["sso_session"] = $session;

//print_r($_SESSION);
//echo $session;
return $json->access_token;

} else {

  return "Something went wrong. Please try again later";
}

}


public function updateToken($refresh) {
  $refreshResult=$this->client->request('POST', 'oauth2/token', [
    'headers' => ['Authorization' => 'Basic '.$this->getAuthorizationCode()],
    'form_params' => [
      'grant_type' => 'refresh_token',
      'refresh_token' => $refresh

    ]
  ]);
  $newTokenJson= json_decode($refreshResult->getBody());

  //echo $newTokenJson;

  //$newTokenJson = json_encode($responseBody);
  echo "new token: ".$newTokenJson->access_token;

  $session = $_SESSION["sso_session"];
  $session->setTokenExpiryTime(time() + $newTokenJson->expires_in);
  $session->setAccessToken($newTokenJson->access_token);
  $session->setRefreshToken($newTokenJson->refresh_token);

  $_SESSION["sso_session"]= $session;

  echo "Session updated";
  return $newTokenJson->access_token;
}

public function redirect() {
  $base_uri='https://stg-sso.dit.gov.bt/oauth2/authorize?';
  $data = array(
    'client_id' => $GLOBALS['CLIENT_ID'],
    'scope' => 'openid',
    'response_type' => 'code',
    'redirect_uri' => $GLOBALS['CALLBACK']

  );


  //echo $base_uri.http_build_query($data);
  session_write_close();
  session_regenerate_id(true);
  header("Location: ".$base_uri.http_build_query($data));
  // session_write_close();
  // session_regenerate_id(true);
  exit;

}



public function getApplicationToken() {

  if(isset($_SESSION["sso_session"])) {
    $session = $_SESSION["sso_session"];
    if($session->checkSession()) {
      echo "Session active. <br/>";
      return $session->getAccessToken();
    } else {
      echo "Generating accessToken from the refresh token";
      $refreshToken = $session->getRefreshToken();
      echo "refresh token:".$refreshToken;

      $updatedToken = $this->updateToken($refreshToken);
      return $updatedToken;
    }
  } else {
    $this->redirect();
  }
}

/**
    * prevent the instance from being cloned (which would create a second instance of it)
    */
   private function __clone()
   {

   }

   /**
    * prevent from being unserialized (which would create a second instance of it)
    */
   private function __wakeup()
   {

   }


}






 ?>
