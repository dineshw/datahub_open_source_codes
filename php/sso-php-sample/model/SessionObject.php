<?php
namespace model;

class SessionObject {

private $accessToken;
private $refreshToken;
private $expiryTime;
private $userName;
private $role;
private $firstName;
private $middleName;
private $lastName;
private $mobileNo;
private $email;
private $authorizedCID;



function getAccessToken() {

return $this->accessToken;

}

function setAccessToken ($accessToken) {

$this->accessToken = $accessToken;

}

function getRefreshToken() {

return $this->refreshToken;

}

function setRefreshToken ($refreshToken) {

$this->refreshToken = $refreshToken;

}

function getTokenExpiryTime() {

  return $this->tokenExpiryTime;
}

function setTokenExpiryTime($tokenExpiryTime) {


//$this->expiryTime =  date("m/d/Y h:i:s a", time() + $tokenExpiryTime);

 $this->expiryTime = $tokenExpiryTime;

// echo $this->expiryTime;


}

function setUserName($userName){$this->userName = $userName;}
function getUserName(){return $this->userName;}
function setRole($role){$this->role = $role;}
function getRole(){return $this->role;}
function setFirstName($firstName){$this->firstName = $firstName;}
function getFirstName(){return $this->firstName;}
function setMiddleName($middleName){$this->middleName = $middleName;}
function getMiddleName(){return $this->middleName;}
function setLastName($lastName){$this->lastName = $lastName;}
function getLastName(){return $this->lastName;}
function setMobileNo($mobileNo){$this->mobileNo = $mobileNo;}
function getMobileNo(){return $this->mobileNo;}
function setEmail($email){$this->email = $email;}
function getEmail(){return $this->email;}
function setAuthorizedCID($authorizedCID){$this->authorizedCID = $authorizedCID;}
function getAuthorizedCID(){return $this->authorizedCID;}



function getExpiryTime() {

  return $this->expiryTime;
}

function tokenTimeValid() {

//echo "expiry time:".$this->expiryTime;

//echo "current time:".time();
  //if($this->expiryTime > date("m/d/Y h:i:s a", time())) {
  if($this->expiryTime > time()) {
    //echo "Token time is valid:";
    return true;
  } else {

    //echo "Token time is invalid";
    return false;
  }
}

function checkSession(){

//echo "accesstoken:".$this->accessToken;

  if(isset($this->accessToken)&&($this->tokenTimeValid())) {
    return true;
  } else return false;

  //return false;

}

}


 ?>
