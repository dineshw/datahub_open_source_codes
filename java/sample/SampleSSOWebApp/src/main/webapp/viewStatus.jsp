<%@page import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CidstatusObj"%>
<%@page import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CidstatusResponse"%>
<%@page import="bt.gov.ditt.sso.client.ApplicationTokenClient"%>
<%@page import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CitizendetailsObj"%>
<%@page
	import="org.wso2.client.model.DCRC_CitizenDetailsAPI.CitizenDetailsResponse"%>
<%@page import="org.wso2.client.api.DCRC_CitizenDetailsAPI.DefaultApi"%>
<%@page import="org.wso2.client.api.ApiClient"%>
<%@page import="bt.gov.ditt.sso.client.SSOClientConstants"%>
<%@page import="bt.gov.ditt.sso.client.dto.UserSessionDetailDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Citizen Status</title>
</head>

<body>
<%
	String cid = request.getParameter("cid");

	ApiClient apiClient = new ApiClient();
	apiClient.setBasePath("https://stg-api.dit.gov.bt/dcrc_citizen_details_api/1.0.0");
	apiClient.setAccessToken(ApplicationTokenClient.getApplicationToken());

	DefaultApi api = new DefaultApi(apiClient);
	CidstatusResponse resp = api.cidstatusCidGet(cid);

	if (resp.getCidStatusResponse() != null && resp.getCidStatusResponse().getCidStatus()!=null && !resp.getCidStatusResponse().getCidStatus().isEmpty()) {
		CidstatusObj cids = resp.getCidStatusResponse().getCidStatus().get(0);
		
		out.println("CID" + cid + " is " + cids.getStatus() + " and expires on " + cids.getExpirationDate());
		
	}
%>
	<br>
</body>
</html>






